<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Register</title>
  <link href="https://fonts.googleapis.com/css?family=Karla:400,700&display=swap" rel="stylesheet">
  <link rel="stylesheet" href="https://cdn.materialdesignicons.com/4.8.95/css/materialdesignicons.min.css">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
  <link rel="stylesheet" href="{{ asset('') }}assets/css/login.css">
</head>
<body>
  <main>
    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-6 login-section-wrapper">
          <div class="brand-wrapper">
            <img src="assets/images/logo.svg" alt="logo" class="logo">
          </div>
          <div class="login-wrapper my-auto">
            <h1 class="login-title">Register</h1>
            <form action="/signup" method="POST">
                @csrf
              <div class="form-group">
                <label for="nama">Nama</label>
                <input type="text" name="nama" id="nama" class="form-control" placeholder="enter your name" required>
              </div>
              <div class="form-group">
                <label for="nim">Nim</label>
                <input type="text" name="nim" id="nim" class="form-control" placeholder="enter your nim" required>
              </div>
              <div class="form-group">
                <label for="nohp">No Handphone</label>
                <input type="text" name="nohp" id="nohp" class="form-control" placeholder="enter your phone number" required>
              </div>
              <div class="form-group">
                <label for="prodi">Program Studi</label>
                <select name="prodi" id="prodi" class="form-control">
                  @foreach ($prodi as $key => $value)
                    <option value="{{ $value->id_prodi }}">{{ $value->nama_prodi }}</option>
                  @endforeach
                </select>
              </div>
              <div class="form-group mb-4">
                <label for="password">Password</label>
                <input type="password" name="password" id="password" class="form-control" placeholder="enter your passsword" required>
              </div>
              <input name="register" id="register" class="btn btn-block login-btn" type="submit" value="Register">
            </form>
          </div>
        </div>
        <div class="col-sm-6 px-0 d-none d-sm-block">
          <img src="assets/images/login.jpg" alt="login image" class="login-img">
        </div>
      </div>
    </div>
  </main>
  <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
</body>
</html>
