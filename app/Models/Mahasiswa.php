<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Mahasiswa extends Model
{
    use HasFactory;
    protected $table = 'mahasiswa';
    protected $fillable = [
        'nama',
        'nim',
        'no_hp',
        'password',
        'prodi'
    ];
    protected $primaryKey = 'id_mahasiswa';

    public function prodis()
    {
        return $this->belongsTo(Prodi::class, 'prodi');
    }
}
