<?php

namespace App\Http\Controllers;

use App\Models\Admin;
use App\Models\Mahasiswa;
use App\Models\Prodi;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    public function login()
    {
        return view('admin.login');
    }

    public function index()
    {
        $data['prodi'] = Prodi::all();
        $data['mahasiswa'] = Mahasiswa::all();
        return view('admin.home', $data);
    }

    public function prodi()
    {
        $data['prodi'] = Prodi::all();
        return view('admin.prodi', $data);
    }

    public function tambahMahasiswa(Request $request)
    {
        $nim = $request->nim;
        $nama = $request->nama;
        $password = $request->password;
        $no_hp = $request->no_hp;
        $prodi = $request->prodi;

        $mahasiswa = new Mahasiswa;
        $mahasiswa->nim = $nim;
        $mahasiswa->nama = $nama;
        $mahasiswa->password = $password;
        $mahasiswa->no_hp = $no_hp;
        $mahasiswa->prodi = $prodi;
        $mahasiswa->save();

        return redirect('admin/home');
    }

    public function updateMahasiswa(Request $request)
    {
        $id_mahasiswa = $request->id_mahasiswa;
        $nim = $request->nim;
        $nama = $request->nama;
        $password = $request->password;
        $no_hp = $request->no_hp;
        $prodi = $request->prodi;

        Mahasiswa::where('id_mahasiswa', $id_mahasiswa)
            ->update([
                'nim' => $nim,
                'nama' => $nama,
                'password' => $password,
                'no_hp' => $no_hp,
                'prodi' => $prodi
            ]);

        return redirect('admin/home');
    }

    public function deleteMahasiswa($id_mahasiswa)
    {
        Mahasiswa::where('id_mahasiswa', $id_mahasiswa)
            ->delete();

        return redirect('admin/home');
    }

    public function tambahprodi(Request $request)
    {
        $nama_prodi = $request->nama_prodi;

        Prodi::create([
            'nama_prodi' => $nama_prodi
        ]);
        return redirect('admin/prodi');
    }

    public function updateprodi(Request $request)
    {
        $id_prodi = $request->id_prodi;
        $nama_prodi = $request->nama_prodi;

        Prodi::where('id_prodi', $id_prodi)
            ->update([
                'nama_prodi' => $nama_prodi
            ]);

        return redirect('admin/prodi');
    }

    public function deleteprodi($id_prodi)
    {
        Prodi::where('id_prodi', $id_prodi)
            ->delete();

        return redirect('admin/prodi');
    }
}
