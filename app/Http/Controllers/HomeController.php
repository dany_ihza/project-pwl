<?php

namespace App\Http\Controllers;

use App\Models\Mahasiswa;
use App\Models\Prodi;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index()
    {   
        $data['prodi'] = Prodi::all();
        $data['mahasiswa'] = Mahasiswa::all();
        // dd($data['prodi'][3]->mahasiswa);
        return view('home', $data);
    }
}
