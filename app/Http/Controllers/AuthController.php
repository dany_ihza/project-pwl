<?php

namespace App\Http\Controllers;

use App\Models\Mahasiswa;
use App\Models\Prodi;
use App\Models\Admin;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function login()
    {
        return view('login');
    }

    public function signin(Request $request)
    {
        $nim = $request->nim;
        $password = $request->password;

        $mahasiswa = Mahasiswa::where('nim', $nim)->first();
        if ($mahasiswa) {
            if ($password == $mahasiswa->password) {
                $data_login = [
                    'id' => $mahasiswa->id,
                    'nim' => $mahasiswa->nim,
                    'nama' => $mahasiswa->nama
                ];
                session([
                    'login-data' => $data_login
                ]);
                return redirect('home/dashboard');
            } else {
                return redirect('login')->with('error', 'Password Salah');
            }
        } else {
            return redirect('login')->with('error', 'Mahasiswa dengan nim tersebut tidak ditemukan');
        }
    }

    public function register(){
        $data['prodi'] = Prodi::all();
        return view('register', $data);
    }

    public function signup(Request $request){
        $nama = $request->nama;
        $nim = $request->nim;
        $no_hp = $request->nohp;
        $prodi = $request->prodi;
        $password = $request->password;
        
        Mahasiswa::create([
            'nama'      => $nama,
            'nim'       => $nim,
            'no_hp'     => $no_hp,
            'prodi'     => $prodi,
            'password'  => $password
        ]);
        return redirect('login');
    }

    public function logout()
    {
        session()->forget('login-data');
        session()->forget('roles');
        return redirect('login');
    }

    public function loginadmin()
    {
        return view('admin.login');
    }

    public function signinadmin(Request $request)
    {
        $username = $request->username;
        $password = $request->password;

        $admin = Admin::where('username', $username)->first();
        if ($admin) {
            if ($password == $admin->password) {
                $data_login = [
                    'id' => $admin->id,
                    'username' => $admin->username
                ];
                $roles = $admin->role;
                session([
                    'login-data' => $data_login,
                    'roles' => $roles
                ]);
                return redirect('/admin/home');
            } else {
                return redirect('login')->with('error', 'Password Salah');
            }
        } else {
            return redirect('login')->with('error', 'Mahasiswa dengan nim tersebut tidak ditemukan');
        }
    }
}
