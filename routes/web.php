<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\AdminController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [AuthController::class, 'login']);

Route::get('/login', [AuthController::class, 'login']);
Route::post('/signin', [AuthController::class, 'signin']);
Route::get('/register', [AuthController::class, 'register']);
Route::post('/signup', [AuthController::class, 'signup']);
Route::get('/logout', [AuthController::class, 'logout']);

Route::get('/loginadmin', [AuthController::class, 'loginadmin']);
Route::post('/signinadmin', [AuthController::class, 'signinadmin']);

Route::group(['prefix' => 'admin', 'middleware' => 'loggedinadmin'], function () {
    Route::get('/home', [AdminController::class, 'index']);
    Route::post('/mahasiswa/tambah', [AdminController::class, 'tambahMahasiswa']);
    Route::post('/mahasiswa/edit', [AdminController::class, 'updateMahasiswa']);
    Route::get('/mahasiswa/delete/{id_mahasiswa}', [AdminController::class, 'deleteMahasiswa']);
    Route::get('/prodi', [AdminController::class, 'prodi']);
    Route::post('/prodi/tambah', [AdminController::class, 'tambahprodi']);
    Route::post('/prodi/edit', [AdminController::class, 'updateprodi']);
    Route::get('/prodi/delete/{id_prodi}', [AdminController::class, 'deleteprodi']);
});

Route::group(['prefix' => 'home', 'middleware' => 'loggedin'], function () {
    Route::get('/dashboard', [HomeController::class, 'index']);
});
